# Voiceroid2.py
# ボイスロイド2をpythonで操作可能にする実装

#imports
import pywinauto

def search_child_byclassname(class_name, uiaElementInfo, target_all = False):
    """子要素の検索

    References:
        https://blog.sky-net.pw/article/78
        https://qiita.com/Teara/items/936733c9e7e47b5ebe79
    """
    target = []
    # 全ての子要素検索
    for childElement in uiaElementInfo.children():
        # ClassNameの一致確認
        if childElement.class_name == class_name:
            if target_all == False:
                return childElement
            else:
                target.append(childElement)
    if target_all == False:
        # 無かったらFalse
        return False
    else:
        return target

def search_child_byname(name, uiaElementInfo):
    """子要素の検索

    References:
        https://blog.sky-net.pw/article/78
        https://qiita.com/Teara/items/936733c9e7e47b5ebe79
    """
    # 全ての子要素検索
    for childElement in uiaElementInfo.children():
        # Nameの一致確認
        if childElement.name == name:
            return childElement
    # 無かったらFalse
    return False

# ボイスロイド
# reference : 
# https://blog.sky-net.pw/article/78
# https://qiita.com/Teara/items/936733c9e7e47b5ebe79
class Voiceroid2(object):
    def __init__(self):
        """初期化関数

        Args:
            self (instance): 自身のインスタンス

        References:
            https://blog.sky-net.pw/article/78
            https://qiita.com/Teara/items/936733c9e7e47b5ebe79
        """
        # デスクトップのエレメント
        parentUIAElement = pywinauto.uia_element_info.UIAElementInfo()
        # voiceroidを捜索する
        voiceroid2 = search_child_byname("VOICEROID2",parentUIAElement)
        # *がついている場合
        if voiceroid2 == False:
            voiceroid2 = search_child_byname("VOICEROID2*",parentUIAElement)

        # テキスト要素のElementInfoを取得
        TextEditViewEle = search_child_byclassname("TextEditView",voiceroid2)
        textBoxEle      = search_child_byclassname("TextBox",TextEditViewEle)

        # コントロール取得
        self.textBoxEditControl = pywinauto.controls.uia_controls.EditWrapper(textBoxEle)

        # ボタン取得
        buttonsEle = search_child_byclassname("Button",TextEditViewEle,target_all = True)
        # 再生ボタンを探す
        playButtonEle = ""
        for buttonEle in buttonsEle:
            # テキストブロックを捜索
            textBlockEle = search_child_byclassname("TextBlock",buttonEle)
            if textBlockEle.name == "再生":
                playButtonEle = buttonEle
                break

        # ボタンコントロール取得
        self.playButtonControl = pywinauto.controls.uia_controls.ButtonWrapper(playButtonEle)        

    # def say()
    # テキストボックスにテキストを入力、再生する
    # text : テキストボックスに入力され、再生される文字列
    # return : なし
    # reference : 
    # https://blog.sky-net.pw/article/78
    # https://qiita.com/Teara/items/936733c9e7e47b5ebe79
    def Say(self, text):
        """textをテキストボックスに設定し、それを読み上げる

        Args:
            self (instance): 自身のインスタンス
            test   (string): テキストボックスに設定され読み上げられる文字列
        
        References:
            https://blog.sky-net.pw/article/78
            https://qiita.com/Teara/items/936733c9e7e47b5ebe79
        """
        # テキスト登録
        self.textBoxEditControl.set_edit_text(text)
        # 再生ボタン押下
        self.playButtonControl.click()