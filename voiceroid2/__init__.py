# __init__.py
# GetVoiceroid()だけimportして使用する。

# imports
from .Voiceroid2 import Voiceroid2

# voiceroidインスタンス
voiceroid2 = Voiceroid2()

def GetVoiceroid():
    """voiceroidインスタンスの取得

    Returns:
        Voiceroid2: インスタンス
    """
    return voiceroid2