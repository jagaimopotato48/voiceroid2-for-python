# Test.py
# voiceroidライブラリのテスト

# imports
import time
from voiceroid2 import GetVoiceroid

# constants
VOICEROID = GetVoiceroid()

def SayTest():
    """Voiceroid2.Say()の動作を確認する

    Returns:
        bool: テストの成否
    """
    result = True

    print("SayTest(): ")
    try:
        # 正常系のテスト1
        # 文字列を持つ変数が渡される
        test1Text = "Say()のテストです。"
        VOICEROID.Say(test1Text)
        time.sleep(2)

        # 正常系のテスト2
        # リテラルがそのまま渡される
        VOICEROID.Say("Say()のテストです。")
        time.sleep(2)

        # 正常系のテスト3
        # 空文字列を持つ変数が渡される
        test3Text = ""
        VOICEROID.Say(test3Text)
        time.sleep(1)

        # 正常系のテスト4
        # 空文字列のリテラルが渡される
        VOICEROID.Say("")
        time.sleep(1)

    except Exception:
        print("Error")
        result = False
    else:
        print("OK")
    finally:
        pass

    return result

if __name__ == "__main__":
    result = True

    print("voiceroid2Test Start")
    result = SayTest()
    print("voiceroid2Test Finish")

    if result == True:
        print("全テスト成功")
    else:
        print("テストに失敗")

    print("テストを終了します。")